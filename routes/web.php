<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

use Telegram\Bot\Laravel\Facades\Telegram;

Route::get('/', function () {
    $response = Telegram::getMe();
    Telegram::sendMessage([
        'chat_id' => $response->id,
        'text' => 'Hello World'
    ]);
    return view('welcome');
});

Route::post(env('TELEGRAM_BOT_TOKEN').'/webhook', function () {
    $updates = Telegram::commandsHandler(true);
    return 'ok';
});
